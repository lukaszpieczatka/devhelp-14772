module.exports = function (app, addon) {

    let content = `
    <ac:structured-macro ac:name="code" ac:schema-version="1" ac:macro-id="dd05c377-6385-4c02-8790-0fb3dfdf7209"><ac:plain-text-body><![CDATA[<!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>
        </head>
        <body>

        </body>
        </html>]]></ac:plain-text-body>
    </ac:structured-macro>
    `;

    // Render the macro by returning a hello world html message
    app.get('/macro-static', addon.authenticate(), function (req, res) {
            res.send(content);
        }
    );

};
